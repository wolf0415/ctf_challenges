#!/usr/bin/env python3

#### 
# Windows Powershell Operation evnet logs 

numbers = [
    72,
    84,
    66,
    123,
    98,
    51,
    119,
    104,
    52,
    114,
    51,
    95,
    48,
    102,
    95,
    116,
    104,
    51,
    95,
    98,
    48,
    48,
    116,
    53,
    95,
    48,
    102,
    95,
    106,
    117,
    115,
    116,
    49,
    99,
    51,
    46,
    46,
    46,
    125
]
flag = ""
for i in numbers:
    flag += chr(i)

print(flag)