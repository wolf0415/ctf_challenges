1. When you received a broken file, the first thing you do is inspecting the hex with hexeditor to determine the file signature/magic bytes.

    a. In my instance, I used online hex editor to inspect the hex.
    ![](./magic_bytes.png) 
    b. As you can see there is a broken imagine file with the indicator of `JFIF`
2. Search for the relevant file signature through this website: https://www.garykessler.net/library/file_sigs.html

    a. The  file signature of the image is ` `FF D8 FF E0 xx xx 4A 46 49 46 00`
    b. This prompted us to change the file signature according to it so that it becomes a image file. 
3. Modify the file signature and save the file as a new file. 
![](./save_as_new_file.png)
4. After that, open the image and the flag can be found in the image. 