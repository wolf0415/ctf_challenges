```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE NFuseProtocol SYSTEM "NFuse.dtd"><NFuseProtocol version="4.6">
	<RequestReconnectSessionData>
		<Credentials>
			<UserName>administrator</UserName>
			<Password encoding="ctx1">MGGDLPBKNNHILIBNMKGPKHACMCGHLABFNDHGKIANPAFFLNBIPBFEKOALMHGCLEBBOLEOIJCMOMEJJIDNOMEJIJCMPLFOKEABNAHFLIBNNJHMLHBCOIENLBBEPAFFLNBIPBFEIMCJ</Password>
			<Domain type="NT">REDFORCE</Domain>
		</Credentials>
		<ClientName>REDLAPTOP</ClientName>
		<ClientName>REDLAPTOP</ClientName>
		<ServerType>win32</ServerType>
		<ClientType>ica30</ClientType>
		<SessionType>disconnected</SessionType>
		<SessionType>active</SessionType>
	</RequestReconnectSessionData>
</NFuseProtocol>
```
1. Based on the xml file, we need to find the flag. 
2. Open up cyberchef and paste in the long strings in the Password section and search up and select `ctx1` .
![](./ctx1.png)
3. The flag will be showing in here.